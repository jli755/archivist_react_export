# Note

I am not going to maintain this project.

I re organized this process to https://gitlab.com/closer-cohorts1/archivist_export

- [create_new_export](https://gitlab.com/closer-cohorts1/archivist_export/create_new_export): click buttons and download xml
    - If already clicked button, just want to download xml, then use [download_xml](https://gitlab.com/closer-cohorts1/archivist_export/create_new_export)
- [download_text_files](https://gitlab.com/closer-cohorts1/archivist_export/create_new_export): download all text files



## Export from archivist

- click "Create new export" button


- Export xml
    - Clean text:
        - replace &amp;amp;# with &#
        - replace &amp;amp; with &amp;
        - replace &amp;# with &#
        - replace &#160: with &#160;
        - replace &#163< with &#163;<
        - replace &amp;amp;amp;# with &#

     - Remove text <duplicate [n]> from sequence labels


- Export dataset
    - tv.txt, rename to prefix.tvlinking.txt
    - dv.txt, rename to prefix.dv.txt
    - QV (Question Variables) qv.txt, rename to prefix.qvmapping.txt
    - TQ (Topic Questions) tq.txt, rename to prefix.tqlinking.txt

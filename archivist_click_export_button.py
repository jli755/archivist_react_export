#!/usr/bin/env python3

"""
Python 3
    Web scraping using selenium to click the "Export" button
        - from https://closer-archivist.herokuapp.com/admin/instruments/exports
"""

from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
import pandas as pd
import time
import sys
import os

from mylib import get_driver, url_base, archivist_login_all, get_base_url
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.by import By


driver = get_driver()


def click_export_button(df, uname, pw):
    """
    Loop over xml_name dictionary, click 'Export'
    """
    df_base_url = get_base_url(df)
    export_name = pd.Series(df_base_url.base_url.values, index=df.Instrument).to_dict()

    print("Got {} xml names".format(len(export_name)))
    # print(df)
    # print(export_name)

    ok = archivist_login_all(driver, export_name.values(), uname, pw)
    print(ok)

    k = 0
    pre_url = None
    for prefix, url in export_name.items():
        if url:
            print("Prefix number {}".format(k))

            if url != pre_url:
                print('Load : ' + url)
                driver.get(url)
                time.sleep(25)

                # find the input box
                inputElement = driver.find_element(by=By.XPATH, value='//input[@placeholder="Search by prefix (press return to perform search)"]')

            else:
                inputElement = pre_input
                # use backspace to clear text field
                inputElement.send_keys(Keys.CONTROL, 'a')
                inputElement.send_keys(Keys.BACKSPACE)

            print('Search prefix "{}"'.format(prefix))
            inputElement.send_keys(prefix)
            print("done")

            # choose from dropdown to display all results on one page
            select = Select(driver.find_element(By.XPATH, "//select[@aria-label='rows per page']"))

            # select by visible text
            select.select_by_visible_text('All')

            # locate id and link
            #TODO: make this better
            trs = driver.find_elements(by=By.XPATH, value="html/body/div/div/div/div/main/div/div/div/div/table/tbody/tr")

            for i, tr in enumerate(trs):
                # print(i)

                # column 2 is "Prefix"
                xml_prefix = tr.find_elements(by=By.XPATH, value="td")[1].text
                print(xml_prefix)

                # column 5 is "Actions", click on "Create new export"
                #TODO: this is case sensitive
                exportButton = tr.find_element(By.LINK_TEXT, "CREATE NEW EXPORT")
                # print(exportButton)
                if (xml_prefix == prefix):
                    print("Click export button for " + prefix)
                    exportButton.click()
                    time.sleep(5)
                else:
                    print("Not " + prefix)
            pre_url = url
            pre_input = inputElement
        k = k + 1
    driver.quit()


def main():
    uname = sys.argv[1]
    pw = sys.argv[2]
    # prefixes
    df = pd.read_csv("Prefixes_to_export.txt", sep="\t")

    click_export_button(df, uname, pw)


if __name__ == "__main__":
    main()

